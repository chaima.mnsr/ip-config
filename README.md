# IP Config

A service that exposes IP configuration data. 

## Prerequisites
- [Java 8+](https://java.com/de/download)
- [Maven 3.x+](https://maven.apache.org/download.cgi)

## Run

To run the application locally use:
```
mvn spring-boot:run
```

or
```
mvn clean package
java -jar target/ip-config-0.0.1-SNAPSHOT.jar
```

The application will be available at http://localhost:8080 with the following endpoint: http://localhost:8080/ip-configs.

## Test

To trigger unit and integration tests run:

```
mvn clean verify
```

## Package
To create the application package within `target/` directory run:

```
mvn clean package
```

## Deployment

#### Local deployment with docker-compose 

```shell script
docker-compose up 
```

#### Deployment on rancher using docker-compose and rancher-compose

Define environment variables:

```shell script
export RANCHER_URL=<url>
export RANCHER_ACCESS_KEY=<username_of_environment_api_key>
export RANCHER_SECRET_KEY=<password_of_environment_api_key>
```

Run docker-rancher command to deploy the application and the load balancer
```shell script
rancher-compose --file deployment/docker-compose.yml -p stack up
```