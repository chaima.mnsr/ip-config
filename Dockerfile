FROM maven:3.6-jdk-8

EXPOSE 8080

RUN mkdir /opt/service

WORKDIR /opt/service
COPY . /opt/service/
RUN mvn clean package

ENTRYPOINT java -jar target/ip-config-0.0.1-SNAPSHOT.jar