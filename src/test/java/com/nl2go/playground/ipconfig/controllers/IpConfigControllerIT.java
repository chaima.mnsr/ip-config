package com.nl2go.playground.ipconfig.controllers;

import com.nl2go.playground.ipconfig.controllers.models.IpConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class IpConfigControllerIT {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void getIpConfigs() {
		List<IpConfig> ipConfigs = asList(
				this.restTemplate.getForObject("http://localhost:" + port + "/ip-configs",
						IpConfig[].class)
		);

		assertThat(ipConfigs, is(
				asList(
						new IpConfig("127.0.0.1", "mta-prod-1", true),
						new IpConfig("127.0.0.2", "mta-prod-1", false),
						new IpConfig("127.0.0.3", "mta-prod-2", true),
						new IpConfig("127.0.0.4", "mta-prod-2", true),
						new IpConfig("127.0.0.5", "mta-prod-2", false),
						new IpConfig("127.0.0.6", "mta-prod-3", false)
				)
		));
	}
}
